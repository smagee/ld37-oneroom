#ifndef SPRITENODE_H
#define SPRITENODE_H

#include <SFML/Graphics.hpp>

#include "SceneNode.h"

class SpriteNode : public SceneNode
{
	public:
		explicit SpriteNode(const sf::Texture& texture);
		SpriteNode(const sf::Texture& texture, const sf::IntRect& textureRect);

	protected:

	private:
		virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

		sf::Sprite mSprite;
};

#endif // SPRITENODE_H
