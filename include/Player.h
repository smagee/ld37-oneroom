#ifndef PLAYER_H
#define PLAYER_H

#include <map>

#include <SFML/Window.hpp>

#include "Command.h"

class CommandQueue;

class Player
{
	public:
		enum Action
		{
			MoveLeft,
			MoveRight,
			MoveUp,
			MoveDown,
			ActionCount
		};

		Player();

		void handleEvent(const sf::Event& event, CommandQueue& commands);
		void handleRealtimeInput(CommandQueue& commands);

		void assignKey(Action action, sf::Keyboard::Key key);
		sf::Keyboard::Key getAssignedKey(Action action) const;

	protected:

	private:
		void initializeActions();
		static bool isRealtimeAction(Action action);

		std::map<sf::Keyboard::Key, Action> mKeyBinding;
		std::map<Action, Command> mActionBinding;
};

#endif // PLAYER_H
