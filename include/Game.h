#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>

#include "World.h"
#include "Player.h"

class Game : private sf::NonCopyable
{
	public:
		Game();

		void run();
	protected:

	private:
		void processEvents();
		void update(sf::Time elapsedTime);
		void render();

		void handlePlayerInput(sf::Keyboard::Key key, bool isPressed);

		static const sf::Time TimePerFrame;

		sf::RenderWindow mWindow;
		World mWorld;
		Player mPlayer;
};

#endif // GAME_H
